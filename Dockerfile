FROM python:3.9-alpine

ARG ANSIBLE_VERSION=2.10.7

WORKDIR /ansible

RUN apk add --no-cache gcc musl-dev libffi-dev openssh \
    && pip install --no-cache-dir ansible==${ANSIBLE_VERSION}

ENTRYPOINT [ "ansible-playbook" ]
