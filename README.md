# Ansible Playbook in Docker

Simple image based on python alpine to run ansible playbooks in docker.

## Example usage

```shell
docker run -v /path/to/project:/ansible/ -v ~/.ssh/id_rsa:/root/.ssh/id_rsa ansible-playbook -i inventory my_playbook.yaml
```
